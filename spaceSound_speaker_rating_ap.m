%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%音源特定課題
%2020/7/9
%Passive vs active block
%練習はそれぞれのブロックの前で行う
%passiveの練習は音源１−１２を順番に流す
%Active blockにおいて、self / pitch-shifted条件をランダマイズする
%[[NEW]] active blockの各試行の音源回答の後、
%音声に対するsoa ratingを回答してもらう
%[[new]] active blockの試行数を減らした。24試行->8試行, first 8 trials
%
%音源位置：4方向3距離の計12条件
%各条件５回繰り返し？（とりあえず）
%2020/7/31 変更内容：
%４方向から２方向
%３距離から２距離
%繰り返し回数5から１０
%2020/8/12
%音声タイプをすべて「あ」に変更
%
%2020/9/16 変更内容：
%active passiveを追加
%ピッチシフトに+4を追加
%繰り返し回数 ピッチシフトなし:8 ピッチ+4:4 ピッチ-4:4 (total 64試行) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Create input objects
deviceReader = audioDeviceReader;

%%
Screen('Preference', 'TextRenderer', 0);

%被験者名
clear SubName;
SubName = input(['Input name of participant:'], 's');
if isempty(SubName)
    return;
end

%性別
clear gender;
gender = input(['Input gender of participant (f/m):'], 's');
if isempty(gender) || ~ (strcmp(gender, 'f') || strcmp(gender, 'm'))
    return;
end

%ブロックタイプ（passive vs active）
clear blockTypeMsg;
blockTypeMsg = input(['Input block type (p/a/ap):'], 's');
if isempty(blockTypeMsg)
    return;
elseif strcmp(blockTypeMsg, 'p')
    blockType = 1; %passive
elseif strcmp(blockTypeMsg, 'a')
    blockType = 2; %active
elseif strcmp(blockTypeMsg, 'ap')
    blockType = 3; %active-passive
else
    return;
end

%練習であるかどうか
clear isPracticeMsg;
isPracticeMsg = input(['is this a practice? (y/n):'], 's');
if isempty(isPracticeMsg)
    return;
elseif strcmp(isPracticeMsg, 'y')
    isPractice = true;
elseif strcmp(isPracticeMsg, 'n')
    isPractice = false; 
else
    return;
end

% home path
home_path = pwd;
%パラメータを設定する
repeats = 5; %各ブロックにおける各条件の繰り返し回数
practiceRepeats = 2;
activeRepeats = 8; %active条件の繰り返し回数
pitchShiftedRepeats = 4; %ピッチシフト条件の繰り返し回数
bgColor = [255 255 255];
stimuliColor = [0 0 0];
fontSize = 30;
soundTypeSum = 5;
pitchUp = 4;
pitchDown = -4;
soundDirection = 2; %方向の数
soundDistance = 2; %距離の数
soundDuration = 3; %音声が再生/発話の秒数
%試行を定義
recordTitleStr = 'participant,block type,trial no.,direction,distance,soundType,passive active,pitch shift,soundsource response,direction response,distance response,rating';
trial = struct('direction', 0, 'distance', 0, 'soundType', 0, 'passiveActive', 0, 'pitchShift', 0, 'directionResponse', 0, 'distanceResponse', 0, 'rating',0);
if blockType == 1 %passive
    if isPractice
        trialSum = practiceRepeats * soundDirection * soundDistance;
    else
        trialSum = repeats * soundDirection * soundDistance;
    end
elseif (blockType == 2 || blockType == 3) %active or active-passive
    if isPractice
        trialSum = practiceRepeats * soundDirection * soundDistance * 2;
    else
        trialSum = (activeRepeats + pitchShiftedRepeats * 2) * soundDirection * soundDistance;
    end
end
trialList = repmat(trial, 1, trialSum); 
%各試行のパラメータを定義する
for i = 1:trialSum
    tmpInt = mod(i, soundDirection * soundDistance);
    if tmpInt == 0
        tmpInt = soundDirection * soundDistance; %1234 1234
    end
    tmpDisInt = mod(tmpInt, soundDistance);
    if tmpDisInt == 0
        tmpDisInt = soundDistance; %12 12 12 12
    end
    tmpDirInt = floor((tmpInt-1)/soundDistance) + 1; %11 22 11 22
    trialList(i).direction = tmpDirInt;
    trialList(i).distance = tmpDisInt;
    %sound type a/i/u/e/o is randomized
    %trialList(i).soundType = mod(round(rand(1)*soundTypeSum), soundTypeSum) + 1;    %random number between 1-soundTypeSum
    trialList(i).soundType = 1;
    if (blockType == 2 || blockType == 3) 
        trialList(i).passiveActive = 1; %0=passive, 1=active(active-passive含む)
        if i > activeRepeats * soundDirection * soundDistance %pitch-shift
            if i > (activeRepeats + pitchShiftedRepeats) * soundDirection * soundDistance
                trialList(i).pitchShift = pitchUp;
            else
                trialList(i).pitchShift = pitchDown;
            end
        end        
    end
end

%if (~isPractice) || (blockType == 2) %本番またはactive blockの練習の時にrandomizeする
randomInd = randperm(trialSum);
trialList = trialList(randomInd);
%end

if isPractice && (blockType == 2 || blockType == 3) %active & practice
    trialSum = 8;
end

%キーボードの設定
% Enable unified mode of KbName, so KbName accepts identical key names on
% all operating systems:
KbName('UnifyKeyNames');
DisableKeysForKbCheck([240, 243, 242, 244]);  
spaceKey = 32; %key code of space
escapeKey = 27; %key to break

try
    % open a new window
    AssertOpenGL;
    % removes the blue screen flash and minimize extraneous warnings.
    Screen('Preference', 'SkipSyncTests', 1);
    Screen('Preference', 'VisualDebugLevel', 3);
    Screen('Preference', 'SuppressAllWarnings', 1);
    
    screenNumber = max(Screen('Screens'));
    [win, rect] = Screen('OpenWindow', screenNumber, bgColor);
    [centerPos(1), centerPos(2)] = RectCenter(rect);
    % duration of each frame
    ifi = Screen('GetFlipInterval', win);
    HideCursor;
    Screen('TextFont',win, 'Arial');
    %Screen('TextFont', win, 'MS Gothic');
    Screen('TextSize',win, fontSize);
    tmpdatestr = datestr(now);
    tmpdatestr = strrep(tmpdatestr,':','-');
    tmpdatestr = strrep(tmpdatestr,' ','_');
    if isPractice
        SaveFileName = [SubName '_' tmpdatestr '_soundsource_practice.csv'];       
    else
        SaveFileName = [SubName '_' tmpdatestr '_soundsource_results.csv'];
    end
    %recording file
    fid = fopen(SaveFileName, 'wt');
    fprintf(fid, '%s\n', recordTitleStr);    
    escapeFlag = false;
    
    if blockType == 1  && isPractice
        msg1 = 'ブロック＃１ 練習';
        msg2 = 'これから４か所の音源からランダムに音声が流れてきます';
        msg3 = '音声の方向と距離をよく聞いてから音源位置を番号で教えてください';
    elseif blockType == 1 && ~isPractice
        msg1 = 'ブロック＃１ 本番';
        msg2 = 'これから４か所の音源からランダムに音声が流れてきます';
        msg3 = '音声の方向と距離をよく聞いてから音源位置を番号で教えてください';
    elseif blockType == 2 && isPractice
        msg1 = 'ブロック＃2 練習';
        msg2 = 'これからのブロックでマイクに向けて画面上に出てくるひらがなを読み上げてください';
        msg3 = '聞こえてくる音声の方向と距離をよく聞いてから音源位置を番号で教えてください';
    elseif blockType == 2 && ~isPractice
        msg1 = 'ブロック＃2 本番';
        msg2 = 'これからのブロックでマイクに向けて画面上に出てくるひらがなを読み上げてください';
        msg3 = '聞こえてくる音声の方向と距離をよく聞いてから音源位置を番号で教えてください';
    elseif blockType == 3  && isPractice
        msg1 = 'ブロック＃３ 練習';
        msg2 = 'これから４か所の音源からランダムに音声が流れてきます';
        msg3 = '音声の方向と距離をよく聞いてから音源位置を番号で教えてください';
    elseif blockType == 3 && ~isPractice
        msg1 = 'ブロック＃３ 本番';
        msg2 = 'これから４か所の音源からランダムに音声が流れてきます';
        msg3 = '音声の方向と距離をよく聞いてから音源位置を番号で教えてください';
    end
    DrawTx(win, centerPos(1), centerPos(2) - 100, double(msg1), stimuliColor);
    DrawTx(win, centerPos(1), centerPos(2), double(msg2), stimuliColor);
    DrawTx(win, centerPos(1), centerPos(2) + 100, double(msg3), stimuliColor);
    DrawTx(win, centerPos(1), centerPos(2) + 200, 'Press space to continue', stimuliColor);
    Screen('Flip', win);        
    %waiting for the space key
    flag = true;
    while flag
        [ keyIsDown, timeSecs, keyCode ] = KbCheck;
        if keyIsDown
            if keyCode(spaceKey)
                flag = false;
            elseif keyCode(escapeKey)
                flag = false;
                escapeFlag = true;
            end
            KbReleaseWait;
        end
    end
    Screen('Flip', win);
    if escapeFlag
        disp('escape before start');
        return;
    end
    %音声ファイルを予め読み込む。
    [y1, Fs1] = audioread(strcat(gender, '1.wav')); %あ
    [y2, Fs2] = audioread(strcat(gender, '2.wav')); %い
    [y3, Fs3] = audioread(strcat(gender, '3.wav')); %う
    [y4, Fs4] = audioread(strcat(gender, '4.wav')); %え
    [y5, Fs5] = audioread(strcat(gender, '5.wav')); %お
    [y6, Fs6] = audioread(strcat(SubName, '.wav')); %被験者の「あ」

    %%%%%%%%%%%%%%%%%%%%各試行はここから始まる%%%%%%%%%%%%%%%%%%%%%%%%
    for trialInd = 1:trialSum  %試行数を繰り返す
        if escapeFlag 
            break;
        end 
        if blockType == 2
            deviceWriter = audioDeviceWriter('SampleRate',deviceReader.SampleRate);
        elseif blockType == 1
            switch trialList(trialInd).soundType
                case 1
                    deviceWriter = audioDeviceWriter('SampleRate',Fs1);
                case 2
                    deviceWriter = audioDeviceWriter('SampleRate',Fs2);
                case 3
                    deviceWriter = audioDeviceWriter('SampleRate',Fs3);
                case 4
                    deviceWriter = audioDeviceWriter('SampleRate',Fs4);
                case 5
                    deviceWriter = audioDeviceWriter('SampleRate',Fs5);
            end
        elseif blockType == 3
            deviceWriter = audioDeviceWriter('SampleRate',Fs6);
        end
        disp(strcat('sound source:', num2str((trialList(trialInd).direction-1)*soundDistance+trialList(trialInd).distance), ' (',num2str(trialList(trialInd).direction),'-', num2str(trialList(trialInd).distance),')'));
        %新しい試行のため，変数たちを初期化する
        HideCursor;
        if isPractice
            msg1 = strcat('練習:', num2str(trialInd), '/', num2str(trialSum));
        else
            msg1 = strcat(num2str(trialInd), '/', num2str(trialSum));
        end
        switch trialList(trialInd).soundType
           case 1
              msg2 = 'あ';
           case 2
              msg2 = 'い';
           case 3
              msg2 = 'う';
           case 4
              msg2 = 'え';
           case 5
              msg2 = 'お';         
        end
        msg3 = '目を閉じてください';
        
        DrawTx(win, centerPos(1), centerPos(2) - 100, double(msg1), stimuliColor);
        DrawTx(win, centerPos(1), centerPos(2), double(msg2), stimuliColor);
        DrawTx(win, centerPos(1), centerPos(2) + 100, double(msg3), stimuliColor);
        DrawTx(win, centerPos(1), centerPos(2) + 200, 'Press space to continue', stimuliColor);
        Screen('Flip', win);        
        %waiting for the space key
        flag = true;
        while flag
            [ keyIsDown, timeSecs, keyCode ] = KbCheck;
            if keyIsDown
                if keyCode(spaceKey)
                    flag = false;
                elseif keyCode(escapeKey)
                    flag = false;
                    escapeFlag = true;
                end
                KbReleaseWait;
            end
        end
        Screen('Flip', win);   
        if escapeFlag
            break;
        end
        
        WaitSecs(0.5);
       
        %ここで音声を流すコード、または音声を処理して流すコードを入れる      
        if blockType == 1 %passive
           switch trialList(trialInd).soundType
               case 1
                  leftChannel = processSound_speaker(trialList(trialInd).pitchShift, y1, Fs1);
                  rightChannel = leftChannel;
                  deviceWriter([leftChannel,rightChannel]);
               case 2
                 % deviceWriter = audioDeviceWriter('SampleRate',Fs2);
                  leftChannel = processSound_speaker(trialList(trialInd).pitchShift, y2, Fs2);
                  rightChannel = leftChannel;
                  deviceWriter([leftChannel,rightChannel]);
               case 3
                  %deviceWriter = audioDeviceWriter('SampleRate',Fs3);
                  leftChannel = processSound_speaker(trialList(trialInd).pitchShift, y3, Fs3);
                  rightChannel = leftChannel;
                  deviceWriter([leftChannel,rightChannel]);
               case 4
                  %deviceWriter = audioDeviceWriter('SampleRate',Fs4);
                  leftChannel = processSound_speaker(trialList(trialInd).pitchShift, y4, Fs4);
                  rightChannel = leftChannel;
                  deviceWriter([leftChannel,rightChannel]);
               case 5
                  %deviceWriter = audioDeviceWriter('SampleRate',Fs5);
                  leftChannel = processSound_speaker(trialList(trialInd).pitchShift, y5, Fs5);
                  rightChannel = leftChannel;
                  deviceWriter([leftChannel,rightChannel]);
           end
           WaitSecs(soundDuration);
           
        elseif blockType == 3 %active-passive
           switch trialList(trialInd).soundType
               case 1
                  leftChannel = processSound_speaker(trialList(trialInd).pitchShift, y6, Fs6);
                  rightChannel = leftChannel;
                  deviceWriter([leftChannel,rightChannel]);
               %{
               case 2
                 % deviceWriter = audioDeviceWriter('SampleRate',Fs2);
                  leftChannel = processSound_speaker(trialList(trialInd).pitchShift, y7, Fs7);
                  rightChannel = leftChannel;
                  deviceWriter([leftChannel,rightChannel]);
               case 3
                  %deviceWriter = audioDeviceWriter('SampleRate',Fs3);
                  leftChannel = processSound_speaker(trialList(trialInd).pitchShift, y8, Fs8);
                  rightChannel = leftChannel;
                  deviceWriter([leftChannel,rightChannel]);
               case 4
                  %deviceWriter = audioDeviceWriter('SampleRate',Fs4);
                  leftChannel = processSound_speaker(trialList(trialInd).pitchShift, y9, Fs9);
                  rightChannel = leftChannel;
                  deviceWriter([leftChannel,rightChannel]);
               case 5
                  %deviceWriter = audioDeviceWriter('SampleRate',Fs5);
                  leftChannel = processSound_speaker(trialList(trialInd).pitchShift, y10, Fs10);
                  rightChannel = leftChannel;
                  deviceWriter([leftChannel,rightChannel]);
               %}
           end
           WaitSecs(soundDuration);
            
        else %active
            tic
            while toc < soundDuration
                mySignal = deviceReader();
                leftChannel = processSound_speaker(trialList(trialInd).pitchShift, mySignal, deviceReader.SampleRate);
                rightChannel = leftChannel;
                deviceWriter([leftChannel,rightChannel]);
            end
                clearvars mySignal; %録音データの初期化
        end
        
        %音源のマップを表示する
        %orange head
        headColor = [237 125 49]; %orange
        Screen('FillOval', win, headColor, [-100+centerPos(1), -100+centerPos(2), 100+centerPos(1), 100+centerPos(2)]);  
        trianglePointList = [-20+centerPos(1) -90+centerPos(2)
                             20+centerPos(1) -90+centerPos(2)
                             0+centerPos(1) -130+centerPos(2)];
        Screen('FillPoly', win, headColor, trianglePointList);
        circles = [200 500];
        circlePenSize = 3;
        for circleInd = 1:soundDistance
            Screen('FrameOval', win, stimuliColor, [-circles(circleInd)+centerPos(1), -circles(circleInd)+centerPos(2), circles(circleInd)+centerPos(1), circles(circleInd)+centerPos(2)], circlePenSize);
        end
        sourceDotSize = 20;
        for dirInd = 1:soundDirection
            for disInd = 1:soundDistance
                switch dirInd
                    case 1
                        dirArr = [-1 -1 -1 -1];
                        textArr = [sourceDotSize*2, -sourceDotSize*2];
                    case 2
                        dirArr = [1 -1 1 -1];
                        textArr = [sourceDotSize*2, -sourceDotSize*2];
                    case 3
                        dirArr = [-1 1 -1 1];
                        textArr = [sourceDotSize*2, -sourceDotSize*2];
                    case 4
                        dirArr = [1 1 1 1];
                        textArr = [sourceDotSize*2, -sourceDotSize*2];
                end
                dotRec = round([cos(pi/4)*circles(disInd), cos(pi/4)*circles(disInd), cos(pi/4)*circles(disInd), cos(pi/4)*circles(disInd)]) .* dirArr;
                dotPosition = [dotRec(1)-sourceDotSize/2+centerPos(1), dotRec(2)-sourceDotSize/2+centerPos(2), dotRec(3)+sourceDotSize/2+centerPos(1), dotRec(4)+sourceDotSize/2+centerPos(2)];
                Screen('FillOval', win, stimuliColor, dotPosition);
                textPosition = [cos(pi/4)*circles(disInd)*dirArr(1)+centerPos(1) cos(pi/4)*circles(disInd)*dirArr(2)+centerPos(2)] + textArr;
                DrawTx(win, textPosition(1), textPosition(2), num2str((dirInd-1)*soundDistance+disInd), stimuliColor);
            end
        end
        %reply=Ask(win, 'Input the number', [], [],'GetString',[0, centerPos(2)*2-100, centerPos(1)*2,centerPos(2)*2], 'center', 20); % Accept keyboard input, but don’t show it.
        disp('input sound source now');
        reply=Ask(win,'音源番号を入力してください:',[],[],'GetChar',[0, centerPos(2)*2-100, centerPos(1)*2,centerPos(2)*2], 'center', 20);
        %disp(strcat('sound source response:', reply));
        Screen('Flip', win);
        %show soa rating
        if (blockType == 2 || blockType == 3) %active or active-passive
            msg = 'スピーカーから聞こえた声は、どのぐらい自分が喋った声だと思いましたか？';
            DrawTx(win, centerPos(1), centerPos(2) - 100, double(msg1), stimuliColor);
            ratingWidth = 200;
            for tmpInd = 1:7
                DrawTx(win, centerPos(1) + (tmpInd-4) * ratingWidth, centerPos(2) + 50, num2str(tmpInd), stimuliColor);
            end
            DrawTx(win, centerPos(1)-3*ratingWidth, centerPos(2) + 150, double('自分の声だと'), stimuliColor);
            DrawTx(win, centerPos(1)-3*ratingWidth, centerPos(2) + 220, double('全く思わなかった'), stimuliColor);
            DrawTx(win, centerPos(1)+3*ratingWidth, centerPos(2) + 150, double('自分の声だと'), stimuliColor);
            DrawTx(win, centerPos(1)+3*ratingWidth, centerPos(2) + 220, double('強く思った'), stimuliColor);
            DrawTx(win, centerPos(1), centerPos(2) + 150, double('どちらとも言えない'), stimuliColor);
            disp('input rating now');
            rating=Ask(win,'レーティングを入力してください:',[],[],'GetChar',[0, centerPos(2)*2-100, centerPos(1)*2,centerPos(2)*2], 'center', 20);
            disp(strcat('rating:', rating));
        end
        
        if blockType == 1
            rating = '-1';
        end
        %record the results for each trial
        if ~(strcmp(reply, 'escape') || strcmp(reply, 'esc') || strcmp(reply, 'end') || strcmp(reply, 'terminate'))
            resultsStr = strcat(SubName, ',', ...
                blockTypeMsg, ',',...
                int2str(trialInd), ',', ...
                num2str(trialList(trialInd).direction), ',', ...
                num2str(trialList(trialInd).distance), ',', ...
                num2str(trialList(trialInd).soundType), ',', ...
                num2str(trialList(trialInd).passiveActive), ',', ...
                num2str(trialList(trialInd).pitchShift), ',', ...
                reply);
            if ~isnan(str2double(reply)) %conver input number and determine direction and distance
                soundSourceNumber = floor(str2double(reply));
                if (soundSourceNumber >= 1) && (soundSourceNumber <= 12)
                    tmpDistance = mod(soundSourceNumber, soundDistance);
                    if tmpDistance == 0
                        tmpDistance = soundDistance;
                    end
                    tmpDirection = floor((soundSourceNumber-1)/soundDistance) + 1;
                    resultsStr = strcat(resultsStr, ',', num2str(tmpDirection), ',', num2str(tmpDistance));
                else
                    resultsStr = strcat(resultsStr, ',wrong input, wrong input');
                end
            else
                resultsStr = strcat(resultsStr, ',no input, no input');
            end
            resultsStr = strcat(resultsStr, ',', rating);
            fprintf(fid, '%s\n', resultsStr); 
            
            if trialInd < trialSum
                msg = '次の試行';
            else
                msg = 'この課題は終わりです';
            end
            DrawTx(win, centerPos(1), centerPos(2), double(msg), stimuliColor);
            Screen('Flip', win);
            WaitSecs(0.5);
        end
        
    end
    %%%%%%%%%%%%%%%%%%%%各試行はここで終わり%%%%%%%%%%%%%%%%%%%%%%%%
    
    %close recoding file
    fclose(fid);  
    Screen('CloseAll');
    
catch
    Screen('CloseAll');
    sca;
    psychrethrow(psychlasterror);
end