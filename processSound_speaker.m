function processedSignal = processSound_speaker(pitch, signal, sampleRate)
%% playsound について
% ピッチ（semitone），音声信号，サンプルレート，書き込み先
% を指定して音を鳴らす

%%
[processedSignal,delays,gains] = shiftPitch(signal, pitch, 0, sampleRate);

end

