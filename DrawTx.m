function DrawTx(win, x, y, Tx, Color)
[area, offset]= Screen('TextBounds', win, Tx);
StartX = x - (area(RectRight)/2);
StartY = y - (area(RectBottom)/2);
Screen('DrawText', win, Tx, StartX, StartY, Color);
end
